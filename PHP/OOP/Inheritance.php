<?php

class Human {
    public $name;
    public $age;
    public $gender;

    public function __construct($name, $age, $gender)
    {
        $this->name = $name;
        $this->age = $age;
        $this->gender = $gender;
    }
}

class Employee extends Human{
    public $salary;

    public function __construct($salary, $name, $age, $gender)
    {
        $this->salary = $salary;
        parent::__construct($name, $age, $gender);
    }

    public function __destruct()
    {
        echo '<br> Inheritance Example';
    }

    public function employee_info()
    {
        return "Employee name is: {$this->name}, age: {$this->age}, gender: {$this->gender} and job title is: {$this->salary}";
    }
}

$user = new Employee(2000, 'Abdallah Orabi', '28', 'male');
echo $user->employee_info();
